<?php include('assets/include/header.php') ?>


<!-- Start of Main Page -->
<div class="home-page-wrapper">
    <!-- Start of Header-Slider -->
    <div class="header-slider">
        <div id="carousel-example" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example" data-slide-to="1"></li>
                <li data-target="#carousel-example" data-slide-to="2"></li>
                <li data-target="#carousel-example" data-slide-to="3"></li>
              </ol>

              <div class="carousel-inner">
                    <!-- 1 -->
                    <div class="item active">
                       <div class="slider-overlay">
                           <div class="slider-content">
                               <div class="container">
                                  <h1>Australian Doctors Network</h1>
                                  <h2>Where Doctors Speak our Minds</h2>
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
                                  <a href="#" class="readmore-btn">Read More</a>
                               </div>
                            </div><!-- slider-content -->
                       </div><!-- slider-overlay -->
                       <a href="#"><img src="assets/img/slide-1.jpg" alt="" /></a>
                    </div>

                    <!-- 2 -->
                    <div class="item">
                       <div class="slider-overlay">
                           <div class="slider-content">
                               <div class="container">
                                  <h1>Australian Doctors Network</h1>
                                  <h2>Where Doctors Speak our Minds</h2>
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
                                  <a href="#" class="readmore-btn">Read More</a>
                               </div>
                            </div><!-- slider-content -->
                       </div><!-- slider-overlay -->
                       <a href="#"><img src="assets/img/slide-1.jpg" alt="" /></a>
                    </div>

                    <!-- 3 -->
                    <div class="item">
                       <div class="slider-overlay">
                           <div class="slider-content">
                               <div class="container">
                                  <h1>Australian Doctors Network</h1>
                                  <h2>Where Doctors Speak our Minds</h2>
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
                                  <a href="#" class="readmore-btn">Read More</a>
                               </div>
                            </div><!-- slider-content -->
                       </div><!-- slider-overlay -->
                       <a href="#"><img src="assets/img/slide-1.jpg" alt="" /></a>
                    </div>

                    <!-- 4-->
                    <div class="item">
                       <div class="slider-overlay">
                           <div class="slider-content">
                               <div class="container">
                                  <h1>Australian Doctors Network</h1>
                                  <h2>Where Doctors Speak our Minds</h2>
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
                                  <a href="#" class="readmore-btn">Read More</a>
                               </div>
                            </div><!-- slider-content -->
                       </div><!-- slider-overlay -->
                       <a href="#"><img src="assets/img/slide-1.jpg" alt="" /></a>
                    </div>


                    

              </div><!-- /carousel-inner -->    
        </div><!-- /carousel-example -->
    </div><!-- end of header-slider -->

<!-- ===================================================================================
                            Home Page portfolio
==================================================================================== -->
<div class="home-portfolio">
    <div class="container">
      <div class="row">
        <!-- 1 col-md-4 -->
        <div class="col-md-4 col-sm-4">
          <div class="portfolio">
            <img src="assets/img/products/portfolio-1.jpg" alt="portfolio">
            <div class="porfolio-content">
                <h3>Conferences & Meetings</h3>
                <!-- 1 -->
                <div class="profile">
                    <div class="profile-image"><img src="assets/img/products/profile-1.jpg" alt=""></div>
                    <div class="profile-text">
                        <h5>Lorem Ipsum is simply dummy..</h5>
                        <span class="text-gray">Posted July 14, 2016 by jemmy tmo </span>
                        <p>the printing and typesetting industry. Lo
                           Ipsum has been the industry's.... <a href="">more</a></p>
                    </div>
                </div><!-- /profile -->

                <hr><!-- Line -->

                <!-- 2 -->
                <div class="profile">
                    <div class="profile-image"><img src="assets/img/products/profile-2.jpg" alt=""></div>
                    <div class="profile-text">
                        <h5>Lorem Ipsum is simply dummy..</h5>
                        <span class="text-gray">Posted July 14, 2016 by jemmy tmo </span>
                        <p>the printing and typesetting industry. Lo
                           Ipsum has been the industry's.... <a href="">more</a></p>
                    </div>
                </div><!-- /profile -->

            </div><!-- / porfolio-content-->
           <button type="button" class="view-all-btn">View All</button>  
          </div>
        </div><!-- /col-md-4 -->


        <!-- 2 col-md-4 -->
        <div class="col-md-4 col-sm-4">
          <div class="portfolio">
            <img src="assets/img/products/portfolio-2.jpg" alt="portfolio">
            <div class="porfolio-content">
                <h3>Interview</h3>
                <!-- 1 -->
                <div class="profile">
                    <div class="profile-image"><img src="assets/img/products/profile-3.jpg" alt=""></div>
                    <div class="profile-text">
                        <h5>Lorem Ipsum is simply dummy..</h5>
                        <span class="text-gray">Posted July 14, 2016 by jemmy tmo </span>
                        <p>the printing and typesetting industry. Lo
                           Ipsum has been the industry's.... <a href="">more</a></p>
                    </div>
                </div><!-- /profile -->

                <hr><!-- Line -->

                <!-- 2 -->
                <div class="profile">
                    <div class="profile-image"><img src="assets/img/products/profile-4.jpg" alt=""></div>
                    <div class="profile-text">
                        <h5>Lorem Ipsum is simply dummy..</h5>
                        <span class="text-gray">Posted July 14, 2016 by jemmy tmo </span>
                        <p>the printing and typesetting industry. Lo
                           Ipsum has been the industry's.... <a href="">more</a></p>
                    </div>
                </div><!-- /profile -->

            </div><!-- / porfolio-content-->
           <button type="button" class="view-all-btn">View All</button>  
          </div>
        </div><!-- /col-md-4 -->


        <!-- 3 col-md-4 -->
        <div class="col-md-4 col-sm-4">
          <div class="portfolio">
            <img src="assets/img/products/portfolio-3.jpg" alt="portfolio">
            <div class="porfolio-content">
                <h3>Hospital Profiles</h3>
                <!-- 1 -->
                <div class="profile">
                    <div class="profile-image"><img src="assets/img/products/profile-5.jpg" alt=""></div>
                    <div class="profile-text">
                        <h5>Lorem Ipsum is simply dummy..</h5>
                        <span class="text-gray">Posted July 14, 2016 by jemmy tmo </span>
                        <p>the printing and typesetting industry. Lo
                           Ipsum has been the industry's.... <a href="">more</a></p>
                    </div>
                </div><!-- /profile -->

                <hr><!-- Line -->

                <!-- 2 -->
                <div class="profile">
                    <div class="profile-image"><img src="assets/img/products/profile-6.jpg" alt=""></div>
                    <div class="profile-text">
                        <h5>Lorem Ipsum is simply dummy..</h5>
                        <span class="text-gray">Posted July 14, 2016 by jemmy tmo </span>
                        <p>the printing and typesetting industry. Lo
                           Ipsum has been the industry's.... <a href="">more</a></p>
                    </div>
                </div><!-- /profile -->

            </div><!-- / porfolio-content-->
           <button type="button" class="view-all-btn">View All</button>  
          </div>
        </div><!-- /col-md-4 -->

      </div><!-- /row -->
    </div><!-- /container -->  
</div><!-- end of home-portfolio -->
<!-- ===================================================================================
                            Home Page forum-tabs
==================================================================================== -->                                               
<div class="forum-tabs-wrapper">
  <div class="container">
      <div class="forum-article-heading">
          <h2>FORUMS</h2>
          <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
      </div><!-- end of forum-article-heading  -->

      <!-- Tabs -->
      <div class="forum-tabs">

          <ul class="nav nav-tabs responsive" id="myTab">
            <li><a href="#announcement">Announcement <i class="indicator visible-sm visible-xs  pull-right fa fa-angle-down" aria-hidden="true"></i></a></li>
            <li class="active"><a href="#health">Health in the news <i class="indicator visible-sm visible-xs  pull-right fa fa-angle-down" aria-hidden="true"></i></a></li>
            <li><a href="#chat">General Chat <i class="indicator visible-sm visible-xs  pull-right fa fa-angle-down" aria-hidden="true"></i></a></li>
            <li><a href="#mstudent">Medical Student Talk <i class="indicator visible-sm visible-xs  pull-right fa fa-angle-down" aria-hidden="true"></i></a></li>
            <li><a href="#gchat">General Chat <i class="indicator visible-sm visible-xs  pull-right fa fa-angle-down" aria-hidden="true"></i></a></li>
          </ul>

          <div class="tab-content responsive">
            <!-- 1 -->
            <div class="tab-pane" id="announcement">
              <div class="row">
               <div class="col-md-4">
                <h3>Announcemnet in the News</h3>
                <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type</p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                <a  href="" class="readmore-btn">READ MORE</a>
              </div><!-- /col-4 -->

              <div class="col-md-4">
                <div class="tabs-comments">
                      <p>Lorem Ipsum is simply dummy text of the printing .</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>when an unknown printer took a galley of type and 
                         scrambled it to make</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>but also the leap into electronic typesetting, remain
                      ing essentially unchanged</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>
                </div><!-- /tabs-comments -->
              </div>
              <div class="col-md-4">
                <div class="forum-ad">
                 <a href="">
                   <img src="assets/img/forum-ad.jpg" alt="">
                   <p>About this Ads</p>
                 </a>
                </div>
              </div>
             </div><!-- row --> 
            </div><!-- /1 -->
            <!-- 2 -->
            <div class="tab-pane active" id="health">
             <div class="row">
               <div class="col-md-4">
                <h3>Health in the News</h3>
                <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type</p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                <a  href="" class="readmore-btn">READ MORE</a>
              </div><!-- /col-4 -->

              <div class="col-md-4">
                <div class="tabs-comments">
                      <p>Lorem Ipsum is simply dummy text of the printing .</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>when an unknown printer took a galley of type and 
                         scrambled it to make</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>but also the leap into electronic typesetting, remain
                      ing essentially unchanged</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>
                      
                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>
                </div><!-- /tabs-comments -->
              </div>
              <div class="col-md-4">
                <div class="forum-ad">
                 <a href="">
                   <img src="assets/img/forum-ad.jpg" alt="">
                   <p>About this Ads</p>
                 </a>
                </div>
              </div>
             </div><!-- row --> 
            </div><!-- /2 -->
            <div class="tab-pane" id="chat">
              <div class="row">
               <div class="col-md-4">
                <h3>Gernal Chat</h3>
                <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type</p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                <a  href="" class="readmore-btn">READ MORE</a>
              </div><!-- /col-4 -->

              <div class="col-md-4">
                <div class="tabs-comments">
                      <p>Lorem Ipsum is simply dummy text of the printing .</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>when an unknown printer took a galley of type and 
                         scrambled it to make</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>but also the leap into electronic typesetting, remain
                      ing essentially unchanged</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>
                      
                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>
                </div><!-- /tabs-comments -->
              </div>
              <div class="col-md-4">
                <div class="forum-ad">
                 <a href="">
                   <img src="assets/img/forum-ad.jpg" alt="">
                   <p>About this Ads</p>
                 </a>
                </div>
              </div>
             </div><!-- row --> 
            </div>
            <div class="tab-pane" id="mstudent">
              <div class="row">
               <div class="col-md-4">
                <h3>Medical Student Talk</h3>
                <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type</p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                <a  href="" class="readmore-btn">READ MORE</a>
              </div><!-- /col-4 -->

              <div class="col-md-4">
                <div class="tabs-comments">
                      <p>Lorem Ipsum is simply dummy text of the printing .</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>when an unknown printer took a galley of type and 
                         scrambled it to make</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>but also the leap into electronic typesetting, remain
                      ing essentially unchanged</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>
                      
                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>
                </div><!-- /tabs-comments -->
              </div>
              <div class="col-md-4">
                <div class="forum-ad">
                 <a href="">
                   <img src="assets/img/forum-ad.jpg" alt="">
                   <p>About this Ads</p>
                 </a>
                </div>
              </div>
             </div><!-- row --> 
            </div>
            <div class="tab-pane" id="gchat">
            <!-- 5 -->
              <div class="row">
               <div class="col-md-4">
                <h3>Gernal Chat</h3>
                <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type</p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                <a  href="" class="readmore-btn">READ MORE</a>
              </div><!-- /col-4 -->

              <div class="col-md-4">
                <div class="tabs-comments">
                      <p>Lorem Ipsum is simply dummy text of the printing .</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>when an unknown printer took a galley of type and 
                         scrambled it to make</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>but also the leap into electronic typesetting, remain
                      ing essentially unchanged</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>
                      
                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>
                </div><!-- /tabs-comments -->
              </div>
              <div class="col-md-4">
                <div class="forum-ad">
                 <a href="">
                   <img src="assets/img/forum-ad.jpg" alt="">
                   <p>About this Ads</p>
                 </a>
                </div>
              </div>
             </div><!-- row --> 
            </div>
          </div><!-- tab-contents -->
            <div class="clearfix"></div>
          <a href="" class="gotoforum-btn">Go To our Forums</a>
      </div><!-- /forum-tabs -->
<div class="clearfix"></div>
  </div><!-- /container -->
</div><!-- /forum-tabs -->

<!-- ===================================================================================
                            Home Page Articles
==================================================================================== -->
<div class="articles-wrapper">
  <div class="container">
      <div class="forum-article-heading">
          <h2>ARTICLES</h2>
          <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
      </div><!-- end of forum-article-heading  -->

      <div class="articles-wrap">
          <div class="row">
            <!-- 1 -->
            <div class="col-md-6">
               <div class="article">
                 <div class="article-image">
                   <img src="assets/img/products/article-1.jpg" alt="">
                 </div>
                 <div class="article-content">
                    <h4>when an unknown printer took a galley 
                       of type and scrambled it to make</h4>
                    <span class="text-gray">By Admin / 18 March 2016</span>

                    <p>Vestibulum eget eros elementum, lob
                      ortis libero non, pretium dolor.Vestibu
                      lum eget eros elementum, </p>
                 </div><!-- /article-content -->
                <a href="" class="readmore-btn">Read More</a>
               </div>
            </div><!-- col-6 -->
            <!-- 2 -->
            <div class="col-md-6">
               <div class="article">
                 <div class="article-image">
                   <img src="assets/img/products/article-3.jpg" alt="">
                 </div>
                 <div class="article-content">
                    <h4>when an unknown printer took a galley 
                       of type and scrambled it to make</h4>
                    <span class="text-gray">By Admin / 18 March 2016</span>

                    <p>Vestibulum eget eros elementum, lob
                      ortis libero non, pretium dolor.Vestibu
                      lum eget eros elementum, </p>
                 </div><!-- /article-content -->
                <a href="" class="readmore-btn">Read More</a>
               </div>
            </div><!-- col-6 -->
            <!-- 3 -->
            <div class="col-md-6">
               <div class="article">
                 <div class="article-image">
                   <img src="assets/img/products/article-2.jpg" alt="">
                 </div>
                 <div class="article-content">
                    <h4>when an unknown printer took a galley 
                       of type and scrambled it to make</h4>
                    <span class="text-gray">By Admin / 18 March 2016</span>
                    
                    <p>Vestibulum eget eros elementum, lob
                      ortis libero non, pretium dolor.Vestibu
                      lum eget eros elementum, </p>
                 </div><!-- /article-content -->
                <a href="" class="readmore-btn">Read More</a>
               </div>
            </div><!-- col-6 -->

            <!-- 4 -->
            <div class="col-md-6">
               <div class="article">
                 <div class="article-image">
                   <img src="assets/img/products/article-4.jpg" alt="">
                 </div>
                 <div class="article-content">
                    <h4>when an unknown printer took a galley 
                       of type and scrambled it to make</h4>
                    <span class="text-gray">By Admin / 18 March 2016</span>
                    
                    <p>Vestibulum eget eros elementum, lob
                      ortis libero non, pretium dolor.Vestibu
                      lum eget eros elementum, </p>
                 </div><!-- /article-content -->
                <a href="" class="readmore-btn">Read More</a>
               </div>
            </div><!-- col-6 -->
          </div><!-- /row -->
          <a href="" class="gotoforum-btn">View All Articles</a>
      </div><!-- articles -->
  </div><!-- /container -->
</div><!-- sponsers-wrapper -->
<!-- ===================================================================================
                            Home Page Sponsers
==================================================================================== -->
<div class="sponsers-wrapper">
  <div class="container">
      <div class="forum-article-heading">
          <h2>Partners / Sponsors</h2>
          <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
      </div><!-- end of forum-article-heading  -->

      <div class="sponser-slider">
            <div id="owl-example-1" class="owl-carousel manufacture-slider">
            <!-- 1 -->
            <div class="item darkCyan">
              <div class="sponser-item">
                <img src="assets/img/products/sponser-1.jpg" alt="Sponsers">
              </div>
            </div>

            <!-- 2 -->
            <div class="item darkCyan">
              <div class="sponser-item">
                <img src="assets/img/products/sponser-2.jpg" alt="Sponsers">
              </div>
            </div>

            <!-- 3 -->
            <div class="item darkCyan">
              <div class="sponser-item">
                <img src="assets/img/products/sponser-3.jpg" alt="Sponsers">
              </div>
            </div>

            <!-- 4 -->
            <div class="item darkCyan">
              <div class="sponser-item">
                <img src="assets/img/products/sponser-4.jpg" alt="Sponsers">
              </div>
            </div>

            <!-- 5 -->
            <div class="item darkCyan">
              <div class="sponser-item">
                <img src="assets/img/products/sponser-5.jpg" alt="Sponsers">
              </div>
            </div>

            <!-- 6 -->
            <div class="item darkCyan">
              <div class="sponser-item">
                <img src="assets/img/products/sponser-1.jpg" alt="Sponsers">
              </div>
            </div>

            <!-- 7 -->
            <div class="item darkCyan">
              <div class="sponser-item">
                <img src="assets/img/products/sponser-2.jpg" alt="Sponsers">
              </div>
            </div>

            </div><!-- end of owl-example-1 -->
      </div><!-- end of sponser-slider -->
  </div><!-- /container -->
</div><!-- end of sponsers-wrappers -->

</div><!-- end of home-page-wrapper -->




<?php include('assets/include/footer.php') ?>












