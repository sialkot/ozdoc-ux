<?php include('assets/include/header.php') ?>
<!-- ===============
      BredCrumb                
=================== -->
<div class="ozdoc-bredcrubm-wrapper">
  <div class="container">
    <div class="forum-article-heading">
           <h2>Interview</h2>
    </div>

    <div class="bredcrumb-wrap">
         <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"><a href="">Interview</a></li>        
         </ol>
    </div><!-- /bredcrumb-wrap -->
    
  </div><!-- /container -->
</div><!-- end of ozdoc-bredcrubm-wrapper -->
<!-- ==========================================================
                            Interview Page 
================================================================ -->
<div class="interview-page">
  <div class="container">
    <div class="aboutus-content">
      <div class="row">


       <div class="about-left-content">
        <div class="col-md-8">
           <div class="row">

              <div class="articles-wrapper">
    

                <div class="articles-wrap">
                      <!-- 1 -->
                      <div class="col-md-12">
                         <div class="article">
                           <div class="article-image">
                             <img src="assets/img/products/article-1.jpg" alt="">
                           </div>
                           <div class="article-content">
                              <h4>when an unknown printer took a galley 
                                 of type and scrambled it to make</h4>
                              <span class="text-gray">By Admin / 18 March 2016</span>

                              <p>Vestibulum eget eros elementum, lob ortis libero non, pretium dolo
                                r.Vestibulum eget eros elementum, Vestibulum eget eros elementu
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                tum,Vestibulum eget eros elementum, lob ortis libero non, </p>
                           </div><!-- /article-content -->
                          <a href="" class="readmore-btn">Read More</a>
                         </div>
                         <hr>
                      </div><!-- col-6 -->
                      <!-- 2 -->
                      <div class="col-md-12">
                         <div class="article">
                           <div class="article-image">
                             <img src="assets/img/products/article-2.jpg" alt="">
                           </div>
                           <div class="article-content">
                              <h4>when an unknown printer took a galley 
                                 of type and scrambled it to make</h4>
                              <span class="text-gray">By Admin / 18 March 2016</span>

                              <p>Vestibulum eget eros elementum, lob ortis libero non, pretium dolo
                                r.Vestibulum eget eros elementum, Vestibulum eget eros elementu
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                tum,Vestibulum eget eros elementum, lob ortis libero non, </p>
                           </div><!-- /article-content -->
                          <a href="" class="readmore-btn">Read More</a>
                         </div>
                         <hr>
                      </div><!-- col-6 -->
                      <!-- 3 -->
                      <div class="col-md-12">
                         <div class="article">
                           <div class="article-image">
                             <img src="assets/img/products/article-3.jpg" alt="">
                           </div>
                           <div class="article-content">
                              <h4>when an unknown printer took a galley 
                                 of type and scrambled it to make</h4>
                              <span class="text-gray">By Admin / 18 March 2016</span>

                              <p>Vestibulum eget eros elementum, lob ortis libero non, pretium dolo
                                r.Vestibulum eget eros elementum, Vestibulum eget eros elementu
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                tum,Vestibulum eget eros elementum, lob ortis libero non, </p>
                           </div><!-- /article-content -->
                          <a href="" class="readmore-btn">Read More</a>
                         </div>
                         <hr>
                      </div><!-- col-6 -->

                      <!-- 4 -->
                      <div class="col-md-12">
                         <div class="article">
                           <div class="article-image">
                             <img src="assets/img/products/article-4.jpg" alt="">
                           </div>
                           <div class="article-content">
                              <h4>when an unknown printer took a galley 
                                 of type and scrambled it to make</h4>
                              <span class="text-gray">By Admin / 18 March 2016</span>

                              <p>Vestibulum eget eros elementum, lob ortis libero non, pretium dolo
                                r.Vestibulum eget eros elementum, Vestibulum eget eros elementu
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                tum,Vestibulum eget eros elementum, lob ortis libero non, </p>
                           </div><!-- /article-content -->
                          <a href="" class="readmore-btn">Read More</a>
                         </div>
                         <hr>
                      </div><!-- col-6 -->

                      <!-- 5 -->
                      <div class="col-md-12">
                         <div class="article">
                           <div class="article-image">
                             <img src="assets/img/products/article-5.jpg" alt="">
                           </div>
                           <div class="article-content">
                              <h4>when an unknown printer took a galley 
                                 of type and scrambled it to make</h4>
                              <span class="text-gray">By Admin / 18 March 2016</span>

                              <p>Vestibulum eget eros elementum, lob ortis libero non, pretium dolo
                                r.Vestibulum eget eros elementum, Vestibulum eget eros elementu
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                tum,Vestibulum eget eros elementum, lob ortis libero non, </p>
                           </div><!-- /article-content -->
                          <a href="" class="readmore-btn">Read More</a>
                         </div>
                         <hr>
                      </div><!-- col-6 -->
                </div><!-- articles -->
            </div>
           </div><!-- /row -->

           <div class="interview-pagination">
               <ul class="pagination">
                <li class="active"><a href="#" class="page-link" >1</a></li>
                <li><a href="#" class="page-link" >2</a></li>
                <li><a href="#" class="page-link" >3</a></li>
                <li><a href="#" class="page-link"  aria-label="Next">NEXT</a></li>
              </ul> 


           </div>
          </div><!-- col-8 -->                
        </div><!-- /about-left-content -->
        




        <!-- ozdoc Side Bar -->
        
         <div class="col-md-4">
          <div class="ozdoc-siderbar">
              <div class="sidebar-ad">
                  <a href="">
                   <img src="assets/img/forum-ad.jpg" alt="">
                   <p>About this Ads</p>
                 </a>
              </div><!-- sidebar-ad -->

              <div class="sidebar-forum">
                <div class="forum-article-heading">
                  <h2>FORUMS</h2>
                </div>

                <div class="forum-tabs">
                   <div class="tab-sldier">
                     <ul class="nav nav-tabs " id="myTab">
                        <div id="owl-example-2" class="owl-carousel owl-theme">
                          <div class="item"><li><a href="#announcement">Announcement</a></li></div>
                          <div class="item"><li class="active"><a href="#health">Health in the news</a></li></div>
                          <div class="item"><li><a href="#chat">General Chat</a></li></div>
                          <div class="item"><li><a href="#mstudent">Medical Student</a></li></div>
                          <div class="item"><li><a href="#gchat">General Chat</a></li></div>
                        </div><!-- /owl-example-2 -->
                      </ul>
                       <div class="tab-slider-control">
                         <a class="left slide-tab-left"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                         <a class="right slide-tab-right"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                       </div>
                    </div><!-- end of ab-slider -->
           <div class="tab-content">
            <!-- 1 -->
            <div class="tab-pane" id="announcement">
                <h3>Announcemnet in the News</h3>
                  <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type...<a href="">More</a></p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>


                <div class="tabs-comments">
                      <p>Lorem Ipsum is simply dummy text of the printing .</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>when an unknown printer took a galley of type and 
                         scrambled it to make</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>but also the leap into electronic typesetting, remain
                      ing essentially unchanged</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                     
                </div><!-- /tabs-comments --> 



            </div><!-- tab-pane -->

            <!-- 2 -->
            <div class="tab-pane active" id="health">
                <h3>Health in the News</h3>
                <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type...<a href="">More</a></p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>


                <div class="tabs-comments">
                      <p>Lorem Ipsum is simply dummy text of the printing .</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>when an unknown printer took a galley of type and 
                         scrambled it to make</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>but also the leap into electronic typesetting, remain
                      ing essentially unchanged</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      
                </div><!-- /tabs-comments -->

               </div><!-- tab-pane -->

               <!-- 3 -->
               <div class="tab-pane" id="chat">
                <h3>Gernal Chat</h3>
                  <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type...<a href="">More</a></p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>


                <div class="tabs-comments">
                      <p>Lorem Ipsum is simply dummy text of the printing .</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>when an unknown printer took a galley of type and 
                         scrambled it to make</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>but also the leap into electronic typesetting, remain
                      ing essentially unchanged</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                   


                </div><!-- /tabs-comments -->
               </div><!-- tab-pane -->
               <!-- 4 -->
               <div class="tab-pane" id="mstudent">
                <h3>Medical Student Talk</h3>
                  <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type...<a href="">More</a></p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>


                <div class="tabs-comments">
                      <p>Lorem Ipsum is simply dummy text of the printing .</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>when an unknown printer took a galley of type and 
                         scrambled it to make</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>but also the leap into electronic typesetting, remain
                      ing essentially unchanged</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                     
                </div><!-- /tabs-comments -->
               </div><!-- tab-pane -->
               <!-- 5 -->
               <div class="tab-pane" id="gchat">
                <h3>Gernal Chat</h3>
                  <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type...<a href="">More</a></p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>


                  <div class="tabs-comments">
                        <p>Lorem Ipsum is simply dummy text of the printing .</p>
                        <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                        <p>when an unknown printer took a galley of type and 
                           scrambled it to make</p>
                        <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                        <p>but also the leap into electronic typesetting, remain
                        ing essentially unchanged</p>
                        <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                        <p>Letraset sheets containing Lorem Ipsum passages,</p>
                        <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                        <p>Letraset sheets containing Lorem Ipsum passages,</p>
                        <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                        <p>Letraset sheets containing Lorem Ipsum passages,</p>
                        <span class="text-gray">Updated 09.19.16 by Lony Seana</span>
                  </div><!-- /tabs-comments -->
                 </div><!-- tab-pane -->

               </div><!-- tab-content -->

                </div><!-- dorum-tabs -->
                <a href="" class="gotoforum-btn">Go To our Forums</a>
              </div><!-- end od sidebar-forum -->



              <!-- sideBar -->
              <div class="sidebar-ad">
                  <a href="">
                   <img src="assets/img/about/sider-ad.png" alt="">
                   <p>About this Ads</p>
                 </a>
              </div>







         </div><!-- /col-4 -->
       </div><!-- /aboutus-siderbar -->

















      </div><!-- /row -->
    </div><!-- aboutus-content -->
  </div><!-- /container -->
</div><!-- /aboutus-page -->


<?php include('assets/include/footer.php') ?>
