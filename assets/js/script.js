
/*DropDowen on Hover*/
$(function(){
$(".dropdown").hover(            
function() {
$('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
$(this).toggleClass('open');
$('b', this).toggleClass("caret caret-up");                
},
function() {
$('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
$(this).toggleClass('open');
$('b', this).toggleClass("caret caret-up");                
});
});


//<!-- This is For converting tabs -->
$( 'ul.nav.nav-tabs  a' ).click( function ( e ) {
e.preventDefault();
$( this ).tab( 'show' );
} );
( function( $ ) {
// Test for making sure event are maintained
$( '.js-alert-test' ).click( function () {
alert( 'Button Clicked: Event was maintained' );
} );
fakewaffle.responsiveTabs( [ 'xs', 'sm' ] );
} )( jQuery );



//<!-- Sponser Slider -->
$(document).ready(function($) {
     var owl = $("#owl-example-1"); 
      
     owl.owlCarousel({
        autoPlay : true,
        pagination : false,
        items : 6,
        itemsCustom : false,
        itemsDesktop : [1199, 5],
        itemsDesktopSmall : [991, 4],
        itemsTablet : [768, 3],
        itemsTabletSmall : false,
        itemsMobile : [600, 3],
        itemsMobile : [420, 2],
        singleItem : false,
        itemsScaleUp : false,
      });
});
$("body").data("page", "frontpage");

//<!-- Tab Slider -->
$(document).ready(function($) {
     var owl = $("#owl-example-2"); 
      
     owl.owlCarousel({
        autoPlay : false,
        pagination : false,
        items : 2,
        itemsCustom : false,
        itemsDesktop : [1199, 2],
        itemsDesktopSmall : [991, 4],
        itemsTablet : [768, 3],
        itemsTabletSmall : false,
        itemsMobile : [600, 2],
        itemsMobile : [420, 2],
        singleItem : false,
        itemsScaleUp : false,
      });

      // Custom Navigation Events
      $(".slide-tab-left").click(function(){
        owl.trigger('owl.next');
      })
      $(".slide-tab-right").click(function(){
        owl.trigger('owl.prev');
      })
});
$("body").data("page", "frontpage");




/*arrow chane in panel*/
function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('fa-angle-down fa-angle-up');
}
$('#collapse-myTab').on('hidden.bs.collapse', toggleChevron);
$('#collapse-myTab').on('shown.bs.collapse', toggleChevron);



/*Search Form*/
$(document).ready(function(){

    $('.search-btn').on('click',function(e){
      e.preventDefault();
     $('.fa-search').toggleClass('searchbtn-style');

     $('.hide-form').toggle();
     if ($('.hide-form').is(':visible'))
        $('.hide-form').css('display','inline-block');
    });

});

