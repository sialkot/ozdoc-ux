<!-- ===================================================================================
                            Footer
==================================================================================== -->
<div class="footer-wrapper">
    <div class="container">
      <div class="row">
      <div class="footer-ad"><img src="assets/img/footer-ad.jpg" alt=""></div> 
       <div class="col-md-4 col-sm-3">
         <div class="logo"><img src="assets/img/logo.png" alt="OzDoc"></div>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard. </p>
         <p> <i class="fa fa-map-marker"></i><a href=""> Big Ben Street, E16 3LS, London,<br>
         United Kingdom</a></p>
         <p> <i class="fa fa-envelope"></i><a href=""> support@ozdoc.com</a></p>
         <p> <i class="fa fa-phone"></i>
         <a href="">+5559 9654982 84</a></p>

       </div>
       <div class="col-md-5 col-sm-5">
         <div class="forum-article-heading">
           <h2>Quick Links</h2>
         </div><!-- end of forum-article-heading  -->
         <ul class="footer-links">
           <li><a href="">Home</a></li>
           <li><a href=""> About Us</a></li>
           <li><a href="">Articles</a></li>
           <li><a href="">Forums</a></li>
           <li><a href="">Conferences & Meetings</a></li>
           <li><a href="">Interview</a></li>
           <li><a href="">Communities</a></li>
           <li><a href="">Resources</a></li>
           <li><a href="">Partners</a></li>
           <li><a href="">Health in the News</a></li>
           <li><a href="">Quiz of the week</a></li>
           <li><a href="">Polls</a></li>
           <li><a href="">Staff</a></li>
           <li><a href="">Links</a></li>
           <li><a href=""> Contact us</a></li>
         </ul>

       </div>
       <div class="col-md-3  col-sm-4 text-center">
          <div class="forum-article-heading">
           <h2>Get On Social</h2>
         </div><!-- end of forum-article-heading  -->
         <ul class="socials-icons">
           <li class="fbook"><a href=""><i class="fa fa-facebook"></i></a></li>
           <li class="twitter"><a href=""><i class="fa fa-twitter"></i></a></li>
           <li class="linkdin"><a href=""><i class="fa fa-linkedin"></i></a></li>
           <li class="googleplus"><a href=""><i class="fa fa-google-plus"></i></a></li>
         </ul>
         <div class="staff-members">
           <a href="">
             <div class="staff-membrs-image">
               <img src="assets/img/products/staff.jpg" alt="">
               <div class="overlay-staff"><span class="over-lay-btn">Staff Members</span></div>
             </div>
           </a>
         </div>
       </div>
       </div>
    </div><!-- /container -->

    <footer>
      <div class="container">
          <p><a href="">OzDoc.com</a> opyright 2016-2018. All Rights Reserved.</p>
          <p>Design and Developed By: <a href=""> Karigar Web Solution Company</a></p>
      </div>  
    </footer>
</div><!-- footer-wrapper -->
<!--=============================================Javascript==========================================================================-->
    <!-- jQuery first, then Bootstrap JS. -->
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/responsive.tabs.js"></script>
<script src="assets/js/owl.carousel.js"></script>
<script src="assets/js/script.js"></script>





</body>
</html>