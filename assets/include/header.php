<!DOCTYPE html>
<html lang="en-US">
<head>
        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <!--Mobile Specific Meta  -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <title>OzDoc</title>
        

        <link rel="stylesheet" type="text/css"  href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css"  href="assets/css/font-awesome.min.css">
        <!-- Owl Carousel Assets -->
        <link href="assets/css/owl.carousel.css" rel="stylesheet">
        <link href="assets/css/owl.theme.css" rel="stylesheet">
        <!-- Main StyleSheet -->
        <link rel="stylesheet" type="text/css"  href="assets/css/style.css">
        

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body>
<header>

    <div class="top-navigation">
        <div class="container">
                <ul class="top-nav-links">
                    <li><a href="">Polls</a></li>
                    <li><a href="">Staff</a></li>
                    <li><a href="">Links</a></li>
                    <li><a href="">Health in the News</a></li>
                    <li><a href="">Contact Us</a></li>
                </ul>
        </div><!-- end of container -->
    </div><!-- end of top-navigation -->


    <div class="container">
         <div class="logo-section">
            <div class="logo"><img src="assets/img/logo.png" alt="OzDoc"></div>
                <div class="logo-content">
                    <address>
                            <a href="">Big Ben Street, E16 3LS, London,<br>
                             United Kingdom</a>
                    </address>
                    <i class="fa fa-mobile" aria-hidden="true"></i>
                    <div class="number">
                        <h6>CALL US</h6>
                        <a href="">514 908 9010</a>
                    </div>
                </div>  
         </div><!-- end of logo-section -->
    </div><!-- /container -->



    <!-- Main Navigation -->
  <nav class="navbar navbar-default" >
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
     <!-- 2nd searchform show only on 768 -->
    <div class="navbar-header-searchform">
        <div class="search-form">
        <form  role="search">
          <div class="input-group">
              <input type="text" class="form-control" placeholder="enter youre keywords" name="q">
              <span class="glyphicon glyphicon-search form-control-feedback"></span>
          </div>
        </form>
      </div><!-- /search-form -->
   </div><!-- navbar-header-searchform --> 

    </div><!-- /navbar-header -->
  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li><a href="#"><i class="fa fa-home"></i></a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">ABOUT US</a>
        <!-- <ul class="dropdown-menu">
          <li><a href="#">Action</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li class="divider"></li>
          <li><a href="#">Separated link</a></li>
          <li class="divider"></li>
          <li><a href="#">One more separated link</a></li>
        </ul> -->
      </li>
      <li class="active"><a href="#">ARTICLES</a></li>
      <li><a href="#">FORUMS</a></li>
      <li><a href="#">Conferences & Meetings</a></li>
      <li><a href="#">Interview</a></li>
      <li><a href="#">RESOURCES</a></li>
      <li><a href="#">Partners / Sponsors</a></li>
      <li><a href="#">Hospital Profi</a></li>
      <li><a href="#" class="search-btn"><i class="fa fa-search"></i></a></li>

    </ul>
    <!-- Search Form -->
    <div class="search-form hide-form">
      <form  role="search">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="enter youre keywords" name="q">
        </div>
      </form>
    </div><!-- /search-form -->

  </div><!-- /.navbar-collapse -->
</div><!-- /container -->
</nav>
</header>
