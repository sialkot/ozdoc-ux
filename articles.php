<?php include('assets/include/header.php') ?>

<!-- ===============
      BredCrumb                
=================== -->
<div class="ozdoc-bredcrubm-wrapper">
  <div class="container">
    <div class="forum-article-heading">
           <h2>ArticleS</h2>
    </div>

    <div class="bredcrumb-wrap">
         <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"><a href="">Article</a></li>        
         </ol>
    </div><!-- /bredcrumb-wrap -->
    
  </div><!-- /container -->
</div><!-- end of ozdoc-bredcrubm-wrapper -->
<!-- ===========================================================================================================================
                            Main Page Article
================================================================================================================================= -->
<div class="articles-page">
  <div class="container">
    <div class="aboutus-content">
      <div class="row">


       <div class="about-left-content">
        <div class="col-md-12">
           <div class="row">

              <div class="articles-wrapper">
    
                <div class="articles-wrap">
                      <!-- 1 -->
                      <div class="col-md-6">
                         <div class="article">
                           <div class="article-image">
                             <img src="assets/img/products/article-1.jpg" alt="">
                           </div>
                           <div class="article-content">
                              <h4>when an unknown printer took a galley 
                                 of type and scrambled it to make</h4>
                              <span class="text-gray">By Admin / 18 March 2016</span>

                              <p>Vestibulum eget eros elementum, lob ortis libero non, pretium dolo
                                r.Vestibulum eget eros elementum, Vestibulum eget eros elementu
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                tum,Vestibulum eget eros elementum, lob ortis libero non, </p>
                           </div><!-- /article-content -->
                          <a href="" class="readmore-btn">Read More</a>
                         </div>
                      </div><!-- col-6 -->
                      <!-- 2 -->
                      <div class="col-md-6">
                         <div class="article">
                           <div class="article-image">
                             <img src="assets/img/products/article-2.jpg" alt="">
                           </div>
                           <div class="article-content">
                              <h4>when an unknown printer took a galley 
                                 of type and scrambled it to make</h4>
                              <span class="text-gray">By Admin / 18 March 2016</span>

                              <p>Vestibulum eget eros elementum, lob ortis libero non, pretium dolo
                                r.Vestibulum eget eros elementum, Vestibulum eget eros elementu
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                tum,Vestibulum eget eros elementum, lob ortis libero non, </p>
                           </div><!-- /article-content -->
                          <a href="" class="readmore-btn">Read More</a>
                         </div>
                      </div><!-- col-6 -->
                      <!-- 3 -->
                      <div class="col-md-6">
                         <div class="article">
                           <div class="article-image">
                             <img src="assets/img/products/article-3.jpg" alt="">
                           </div>
                           <div class="article-content">
                              <h4>when an unknown printer took a galley 
                                 of type and scrambled it to make</h4>
                              <span class="text-gray">By Admin / 18 March 2016</span>

                              <p>Vestibulum eget eros elementum, lob ortis libero non, pretium dolo
                                r.Vestibulum eget eros elementum, Vestibulum eget eros elementu
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                tum,Vestibulum eget eros elementum, lob ortis libero non, </p>
                           </div><!-- /article-content -->
                          <a href="" class="readmore-btn">Read More</a>
                         </div>
                      </div><!-- col-6 -->

                      <!-- 4 -->
                      <div class="col-md-6">
                         <div class="article">
                           <div class="article-image">
                             <img src="assets/img/products/article-4.jpg" alt="">
                           </div>
                           <div class="article-content">
                              <h4>when an unknown printer took a galley 
                                 of type and scrambled it to make</h4>
                              <span class="text-gray">By Admin / 18 March 2016</span>

                              <p>Vestibulum eget eros elementum, lob ortis libero non, pretium dolo
                                r.Vestibulum eget eros elementum, Vestibulum eget eros elementu
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                tum,Vestibulum eget eros elementum, lob ortis libero non, </p>
                           </div><!-- /article-content -->
                          <a href="" class="readmore-btn">Read More</a>
                         </div>
                      </div><!-- col-6 -->

                      <!-- 5 -->
                      <div class="col-md-6">
                         <div class="article">
                           <div class="article-image">
                             <img src="assets/img/products/article-5.jpg" alt="">
                           </div>
                           <div class="article-content">
                              <h4>when an unknown printer took a galley 
                                 of type and scrambled it to make</h4>
                              <span class="text-gray">By Admin / 18 March 2016</span>

                              <p>Vestibulum eget eros elementum, lob ortis libero non, pretium dolo
                                r.Vestibulum eget eros elementum, Vestibulum eget eros elementu
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                tum,Vestibulum eget eros elementum, lob ortis libero non, </p>
                           </div><!-- /article-content -->
                          <a href="" class="readmore-btn">Read More</a>
                         </div>
                      </div><!-- col-6 -->

                      <!-- 6 -->
                      <div class="col-md-6">
                         <div class="article">
                           <div class="article-image">
                             <img src="assets/img/products/article-6.jpg" alt="">
                           </div>
                           <div class="article-content">
                              <h4>when an unknown printer took a galley 
                                 of type and scrambled it to make</h4>
                              <span class="text-gray">By Admin / 18 March 2016</span>

                              <p>Vestibulum eget eros elementum, lob ortis libero non, pretium dolo
                                r.Vestibulum eget eros elementum, Vestibulum eget eros elementu
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                tum,Vestibulum eget eros elementum, lob ortis libero non, </p>
                           </div><!-- /article-content -->
                          <a href="" class="readmore-btn">Read More</a>
                         </div>
                      </div><!-- col-6 -->

                      <!-- 7 -->
                      <div class="col-md-6">
                         <div class="article">
                           <div class="article-image">
                             <img src="assets/img/products/article-7.jpg" alt="">
                           </div>
                           <div class="article-content">
                              <h4>when an unknown printer took a galley 
                                 of type and scrambled it to make</h4>
                              <span class="text-gray">By Admin / 18 March 2016</span>

                              <p>Vestibulum eget eros elementum, lob ortis libero non, pretium dolo
                                r.Vestibulum eget eros elementum, Vestibulum eget eros elementu
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                tum,Vestibulum eget eros elementum, lob ortis libero non, </p>
                           </div><!-- /article-content -->
                          <a href="" class="readmore-btn">Read More</a>
                         </div>
                      </div><!-- col-6 -->

                      <!-- 8 -->
                      <div class="col-md-6">
                         <div class="article">
                           <div class="article-image">
                             <img src="assets/img/products/article-9.jpg" alt="">
                           </div>
                           <div class="article-content">
                              <h4>when an unknown printer took a galley 
                                 of type and scrambled it to make</h4>
                              <span class="text-gray">By Admin / 18 March 2016</span>

                              <p>Vestibulum eget eros elementum, lob ortis libero non, pretium dolo
                                r.Vestibulum eget eros elementum, Vestibulum eget eros elementu
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                m, lob ortis libero non, pretium dolor.Vestibulum eget eros elemen
                                tum,Vestibulum eget eros elementum, lob ortis libero non, </p>
                           </div><!-- /article-content -->
                          <a href="" class="readmore-btn">Read More</a>
                         </div>
                      </div><!-- col-6 -->





                </div><!-- articles -->
            </div>
           </div><!-- /row -->

           <div class="interview-pagination">
               <ul class="pagination">
                <li class="active"><a href="#" class="page-link" >1</a></li>
                <li><a href="#" class="page-link" >2</a></li>
                <li><a href="#" class="page-link" >3</a></li>
                <li><a href="#" class="page-link"  aria-label="Next">NEXT</a></li>
              </ul> 


           </div>
          </div><!-- col-8 -->                
        </div><!-- /about-left-content -->
        






















      </div><!-- /row -->
    </div><!-- aboutus-content -->
  </div><!-- /container -->
</div><!-- /aboutus-page -->



<?php include('assets/include/footer.php') ?>
