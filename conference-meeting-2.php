<?php include('assets/include/header.php') ?>

<!-- ===============
      BredCrumb                
=================== -->
<div class="ozdoc-bredcrubm-wrapper">
  <div class="container">
    <div class="forum-article-heading">
           <h2>Conferences & Meetings</h2>
    </div>

    <div class="bredcrumb-wrap">
         <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="">Conferences & Meetings</a></li>        
            <li class="active"><a href="">Medical School</a></li>        
         </ol>
    </div><!-- /bredcrumb-wrap -->
    
  </div><!-- /container -->
</div><!-- end of ozdoc-bredcrubm-wrapper -->
<!-- ===========================================================================================================================
                            Main Page Article
================================================================================================================================= -->
<div class="conference-meeting-2">
  <div class="container">

      <div class="search-catgory-section">
          <div class="selectins-wrap">
              <!-- 1 -->
              <div class="type-category">
                 <p>Type</p>
                  <select class="header-selection">
                      <option>Allopathic Medical School</option>
                      <option>Allopathic Medical School</option>
                      <option>Allopathic Medical School</option> 
                  </select>
              </div>
              <!-- 2 -->
              <div class="name-category">
                  <p>Name Of School (OPTIONAL):</p>
                  <select class="header-selection">
                      <option>Allopathic Medical School</option>
                      <option>Allopathic Medical School</option>
                      <option>Allopathic Medical School</option> 
                  </select>
              </div>

              <button type="submit" name="search" class="search-cat-btn">Search</button>
              <!-- 3 -->
              <div class="ranking-category">
                    <span>Sort by : </span>
                    <select class="header-selection">
                        <option>Ranking</option>
                        <option>Ranking</option>
                        <option>Ranking</option> 
                    </select>
              </div>

           <button type="submit" name="search" class="flow-btn">High To Low</button>
           <div class="clearfix"></div>

            </div><!-- selection-wrap -->
      </div><!-- /search-catgory-section -->

      <hr class="selection-border">


    <div class="school-list-table">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>NAME</th>
                <th>DATE</th>
                <th class="hidden-xs">VENUE</th>
                <th>COST</th>
                <th class="hidden-xs">Description</th>
              </tr>
            </thead>
            <tbody>
                  <!-- 1 -->
                  <tr>
                    <td>
                      <h5>Medical an unknown printer took a galley</h5>
                      <span class="text-gray">Allopathic Medical School</span>
                      <p>Houston, TX </p>
                      <a href="">More Detail</a>
                    </td>
                    <td><div class="date-circle">Sep 22, 2016</div></td>
                    <td class="hidden-xs">Vestibulum eget eros elementum, </td>
                    <td>23,3300$</td>
                    <td class="hidden-xs">
                     <p>Vestibulum eget eros elementum, lob 
                      ortis libero non, pretium dolor.Vestibu
                      lum eget eros elementum,</p></td>
                  </tr>
                  <!-- 2 -->
                  <tr>
                    <td>
                      <h5>Medical an unknown printer took a galley</h5>
                      <span class="text-gray">Allopathic Medical School</span>
                      <p>Houston, TX </p>
                      <a href="">More Detail</a>
                    </td>
                    <td><div class="date-circle">Sep 22, 2016</div></td>
                    <td class="hidden-xs">Vestibulum eget eros elementum, </td>
                    <td>23,3300$</td>
                    <td class="hidden-xs">
                     <p>Vestibulum eget eros elementum, lob 
                      ortis libero non, pretium dolor.Vestibu
                      lum eget eros elementum,</p></td>
                  </tr>

                  <!-- 3 -->
                  <tr>
                    <td>
                      <h5>Medical an unknown printer took a galley</h5>
                      <span class="text-gray">Allopathic Medical School</span>
                      <p>Houston, TX </p>
                      <a href="">More Detail</a>
                    </td>
                    <td><div class="date-circle">Sep 22, 2016</div></td>
                    <td class="hidden-xs">Vestibulum eget eros elementum, </td>
                    <td>23,3300$</td>
                    <td class="hidden-xs">
                     <p>Vestibulum eget eros elementum, lob 
                      ortis libero non, pretium dolor.Vestibu
                      lum eget eros elementum,</p></td>
                  </tr>

                  <!-- 4 -->
                  <tr>
                    <td>
                      <h5>Medical an unknown printer took a galley</h5>
                      <span class="text-gray">Allopathic Medical School</span>
                      <p>Houston, TX </p>
                      <a href="">More Detail</a>
                    </td>
                    <td><div class="date-circle">Sep 22, 2016</div></td>
                    <td class="hidden-xs">Vestibulum eget eros elementum, </td>
                    <td>23,3300$</td>
                    <td class="hidden-xs">
                     <p>Vestibulum eget eros elementum, lob 
                      ortis libero non, pretium dolor.Vestibu
                      lum eget eros elementum,</p></td>
                  </tr>

                  <!-- 5 -->
                  <tr>
                    <td>
                      <h5>Medical an unknown printer took a galley</h5>
                      <span class="text-gray">Allopathic Medical School</span>
                      <p>Houston, TX </p>
                      <a href="">More Detail</a>
                    </td>
                    <td><div class="date-circle">Sep 22, 2016</div></td>
                    <td class="hidden-xs">Vestibulum eget eros elementum, </td>
                    <td>23,3300$</td>
                    <td class="hidden-xs">
                     <p>Vestibulum eget eros elementum, lob 
                      ortis libero non, pretium dolor.Vestibu
                      lum eget eros elementum,</p></td>
                  </tr>


                  <!-- 6 -->
                  <tr>
                    <td>
                      <h5>Medical an unknown printer took a galley</h5>
                      <span class="text-gray">Allopathic Medical School</span>
                      <p>Houston, TX </p>
                      <a href="">More Detail</a>
                    </td>
                    <td><div class="date-circle">Sep 22, 2016</div></td>
                    <td class="hidden-xs">Vestibulum eget eros elementum, </td>
                    <td>23,3300$</td>
                    <td class="hidden-xs">
                     <p>Vestibulum eget eros elementum, lob 
                      ortis libero non, pretium dolor.Vestibu
                      lum eget eros elementum,</p></td>
                  </tr>
              


              
              



            </tbody>
          </table>
    </div> <!-- school-list-table -->





















  </div><!-- /container -->
</div><!-- /conference-meeting-2.html -->
<?php include('assets/include/footer.php') ?>
