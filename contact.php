<?php include('assets/include/header.php') ?>

<!-- ===============
      BredCrumb                
=================== -->
<div class="ozdoc-bredcrubm-wrapper">
  <div class="container">
    <div class="forum-article-heading">
           <h2>contact us</h2>
    </div>

    <div class="bredcrumb-wrap">
         <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"><a href="">contact Us</a></li>        
         </ol>
    </div><!-- /bredcrumb-wrap -->
    
  </div><!-- /container -->
</div><!-- end of ozdoc-bredcrubm-wrapper -->
<!-- ===========================================================================================================================
                            
================================================================================================================================= -->
<div class="contactus-page">
  <div class="container">
     <div class="row">
      <div class="col-md-6">
          <div class="contact-form-wrap">
             <h3>Write to Us If You Have Questions</h3>
             <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ip
                sum has been the industry's standard dummy text ever since the 1500..</p>

                <div class="ozdec-register-form">
                    <form>
                       <div class="row">
                         <div class="col-md-6 col-sm-12"><input type="text" name="name" class="form-input" placeholder="Your Name"></div>
                         <div class="col-md-6 col-sm-12"><input type="email" name="email" class="form-input" placeholder="Email"></div>

                         <div class="col-md-6 col-sm-12"><input type="text" name="numbere" class="form-input" placeholder="Phone"></div>
                         <div class="col-md-6 col-sm-12"><input type="text" name="subject" class="form-input" placeholder="Subjects"></div>

                         <div class="col-md-12"><textarea placeholder="Message"></textarea></div>

                         <div class="col-md-12">
                         <input type="submit" name="submit" class="submit-btn" value="Submit"></div>
                       </div>
                    </form>
                </div>

          </div><!-- /contact-form-wrap -->
      </div><!-- /col-6 -->



      <!-- Right Side of contact us page -->
      <div class="col-md-6">
        <div class="contactus-detail">
         <div class="logo"><img src="assets/img/logo.png" alt="OzDoc"></div>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type  </p>

         <p> <i class="fa fa-map-marker"></i><a href=""> Big Ben Street, E16 3LS, London,<br>
         United Kingdom</a></p>
         <p> <i class="fa fa-envelope"></i><a href=""> support@ozdoc.com</a></p>
         <p> <i class="fa fa-phone"></i>
         <a href="tel: +5559 9654982 84">+5559 9654982 84</a></p>

          

        </div>
      </div><!-- /col-6 -->
     </div><!-- /row --> 
  </div><!-- /container -->



<div class="google-map"> 
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d26081603.29442044!2d-95.677068!3d37.06250000000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1468416138613" 
        style="border:0; width:100%;height:100%;frameborder:0; " allowfullscreen></iframe>
  
    </div>




</div><!-- /contactus-page -->

<?php include('assets/include/footer.php') ?>
