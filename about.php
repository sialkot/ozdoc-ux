<?php include('assets/include/header.php') ?>

<!-- ===============
      BredCrumb                
=================== -->
<div class="ozdoc-bredcrubm-wrapper">
  <div class="container">
    <div class="forum-article-heading">
           <h2>About Us</h2>
    </div>

    <div class="bredcrumb-wrap">
         <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">About</li>        
         </ol>
    </div><!-- /bredcrumb-wrap -->
    
  </div><!-- /container -->
</div><!-- end of ozdoc-bredcrubm-wrapper -->
<!-- ========================================================================================================================
                            About Us  Page 
============================================================================================================================== -->
<div class="aboutus-page">
  <div class="container">
    <div class="aboutus-content">
      <div class="row">


       <div class="about-left-content">
        <div class="col-md-8">
           <div class="row">
             <div class="col-md-8 col-sm-8">
               <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores veritatis et quasi architecto beatae vitae dicta sunt etras sits eos qui.</p>
             </div>
             <div class="col-md-4 col-sm-4">
               <div class="about-img"><img src="assets/img/about/about-img-1.png" alt=""></div>
             </div>
           </div><!-- /row -->
           <br>
         <p>Ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.<span class="text-green">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,</span>  sed quia consequuntur magni dolores veritatis et quasi architecto beatae vitae dicta sunt etras sits eos qui.

         <br><br>
         Ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.
         </p>

         <ul class="about-us-list">
           <li><a href="">Ratione voluptatem sequi nesciunt. Neque porro quisquam est, </a></li>
           <li><a href="">qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia</a></li>
           <li><a href="">non numquam eius modi tempora incidunt ut labore et dolore.</a></li>
           <li><a href="">magnam aliquam quaerat voluptatem.</a></li>
           <li><a href="">Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse.</a></li>
         </ul>

         <p>Cillum dolore eu fugiat nulla.Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. sed quia consequuntur magni dolores veritatis et quasi architecto beatae vitae dicta sunt etras sits eos qui.</p>
         <br>
         <p>
           Ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.Ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. sed quia consequuntur magni dolores veritatis et quasi architecto beatae vitae dicta sunt etras sits eos qui.
         </p>
         <br>
         <p>
           Ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.

           Ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.
         </p>

         <br>
         <p>
           Ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.

           Ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.
         </p>

         <br>
         <p>
           Ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.

           Ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.
         </p>
        </div><!-- /about-left-content -->
        </div><!-- col-8 -->




        <!-- ozdoc Side Bar -->
        
         <div class="col-md-4">
          <div class="ozdoc-siderbar">
              <div class="sidebar-ad">
                  <a href="">
                   <img src="assets/img/forum-ad.jpg" alt="">
                   <p>About this Ads</p>
                 </a>
              </div><!-- sidebar-ad -->

              <div class="sidebar-forum">
                <div class="forum-article-heading">
                  <h2>FORUMS</h2>
                </div>

                <div class="forum-tabs">
                   <div class="tab-sldier">
                     <ul class="nav nav-tabs " id="myTab">
                        <div id="owl-example-2" class="owl-carousel owl-theme">
                          <div class="item"><li><a href="#announcement">Announcement</a></li></div>
                          <div class="item"><li class="active"><a href="#health">Health in the news</a></li></div>
                          <div class="item"><li><a href="#chat">General Chat</a></li></div>
                          <div class="item"><li><a href="#mstudent">Medical Student</a></li></div>
                          <div class="item"><li><a href="#gchat">General Chat</a></li></div>
                        </div><!-- /owl-example-2 -->
                      </ul>
                       <div class="tab-slider-control">
                         <a class="left slide-tab-left"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                         <a class="right slide-tab-right"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                       </div>
                    </div><!-- end of ab-slider -->
           <div class="tab-content">
            <!-- 1 -->
            <div class="tab-pane" id="announcement">
                <h3>Announcemnet in the News</h3>
                  <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type...<a href="">More</a></p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>


                <div class="tabs-comments">
                      <p>Lorem Ipsum is simply dummy text of the printing .</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>when an unknown printer took a galley of type and 
                         scrambled it to make</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>but also the leap into electronic typesetting, remain
                      ing essentially unchanged</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>
                </div><!-- /tabs-comments --> 



            </div><!-- tab-pane -->

            <!-- 2 -->
            <div class="tab-pane active" id="health">
                <h3>Health in the News</h3>
                <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type...<a href="">More</a></p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>


                <div class="tabs-comments">
                      <p>Lorem Ipsum is simply dummy text of the printing .</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>when an unknown printer took a galley of type and 
                         scrambled it to make</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>but also the leap into electronic typesetting, remain
                      ing essentially unchanged</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>
                </div><!-- /tabs-comments -->

               </div><!-- tab-pane -->

               <!-- 3 -->
               <div class="tab-pane" id="chat">
                <h3>Gernal Chat</h3>
                  <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type...<a href="">More</a></p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>


                <div class="tabs-comments">
                      <p>Lorem Ipsum is simply dummy text of the printing .</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>when an unknown printer took a galley of type and 
                         scrambled it to make</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>but also the leap into electronic typesetting, remain
                      ing essentially unchanged</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>


                </div><!-- /tabs-comments -->
               </div><!-- tab-pane -->
               <!-- 4 -->
               <div class="tab-pane" id="mstudent">
                <h3>Medical Student Talk</h3>
                  <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type...<a href="">More</a></p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>


                <div class="tabs-comments">
                      <p>Lorem Ipsum is simply dummy text of the printing .</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>when an unknown printer took a galley of type and 
                         scrambled it to make</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>but also the leap into electronic typesetting, remain
                      ing essentially unchanged</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                      <p>Letraset sheets containing Lorem Ipsum passages,</p>
                      <span class="text-gray">Updated 09.19.16 by Lony Seana</span>
                </div><!-- /tabs-comments -->
               </div><!-- tab-pane -->
               <!-- 5 -->
               <div class="tab-pane" id="gchat">
                <h3>Gernal Chat</h3>
                  <p>Lorem Ipsum is simply dummy Lorem Ipsum ....</p>

                <span class="text-gray">Posted July 14, 2016 by jemmy tmo</span>
                <br>
                <p class="text-darkgray">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type...<a href="">More</a></p>
                <br>
                <p class="text-darkgray">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>


                  <div class="tabs-comments">
                        <p>Lorem Ipsum is simply dummy text of the printing .</p>
                        <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                        <p>when an unknown printer took a galley of type and 
                           scrambled it to make</p>
                        <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                        <p>but also the leap into electronic typesetting, remain
                        ing essentially unchanged</p>
                        <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                        <p>Letraset sheets containing Lorem Ipsum passages,</p>
                        <span class="text-gray">Updated 09.19.16 by Lony Seana</span>

                        <p>Letraset sheets containing Lorem Ipsum passages,</p>
                        <span class="text-gray">Updated 09.19.16 by Lony Seana</span>
                  </div><!-- /tabs-comments -->
                 </div><!-- tab-pane -->

               </div><!-- tab-content -->

                </div><!-- dorum-tabs -->
                <a href="" class="gotoforum-btn">Go To our Forums</a>
              </div><!-- end od sidebar-forum -->



              <!-- sideBar -->
              <div class="sidebar-ad">
                  <a href="">
                   <img src="assets/img/about/sider-ad.png" alt="">
                   <p>About this Ads</p>
                 </a>
              </div>







         </div><!-- /col-4 -->
       </div><!-- /aboutus-siderbar -->

















      </div><!-- /row -->
    </div><!-- aboutus-content -->
  </div><!-- /container -->
</div><!-- /aboutus-page -->


<?php include('assets/include/footer.php') ?>
